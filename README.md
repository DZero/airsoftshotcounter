# AirsoftShotCounter

## Changelog
- 03.05.24  -- Erstellt
- 04.05.24  -- Portiert von Seeedstudio Xiao zu ESP32 SuperMini
- 06.05.24  -- OLED geschrottet (3,3V)
- 11.05.24  -- OLED funktioniert
            -- Lichtschranke angebunden
- 12.05.24  -- Splashscreen erstellt
            -- Bitmap gezeichnet
- 14.05.24  -- Bitmap animiert (aber falschrum)
- 15.05.24  -- Linien für die Optik eingefügt
            -- neue Bilder erstellt
- 18.05.24  -- OneButton eingefügt und getestet
- 21.05.24  -- Intervalle für OneButton geändert
            -- DrawRectangle funktioniert jetzt richtigrum
            -- Schalfenszeit für OLED implementiert
- 24.05.24  -- OneButton Bibliothek rausgeschmissen
            -- Menü überarbeitet